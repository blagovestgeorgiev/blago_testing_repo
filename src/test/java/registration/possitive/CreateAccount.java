package registration.possitive;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.ShopPage;
import pages.ShopRegistrationPage;
import utilities.Browser;
import utilities.CommonVerification;

public class CreateAccount {
    /**
     * Setup and initialize browser
     */
    @BeforeMethod
    public void setUp() {
        Browser.open("chrome");
        Browser.goTo();
    }

    /**
     * Create new shopper account
     */
    @Test
    public void successfulAccountCreation() {
        ShopPage.findRegisterForm();
        ShopRegistrationPage.register("Test", "Test", "testing1@mail.bg", "111111111111", "123345", "12345");
        ShopRegistrationPage.subscribeAndAccept();
        CommonVerification.verifyCreatedAccount("Register Account", "Failed Register Account");
    }

    /**
     * Close browser
     */
    @AfterMethod
    public void tearDown() {
        Browser.quit();
    }
}
