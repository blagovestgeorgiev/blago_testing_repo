package negative;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.ShopPage;
import pages.ShopRegistrationPage;
import utilities.Browser;
import utilities.CommonVerification;

public class CreateAccount {/**
 * Setup and initialize browser
 */
@BeforeMethod
public void setUp() {
    Browser.open("chrome");
    Browser.goTo();
}

    /**
     * Create new shopper account
     */
    @Test
    public void unsuccessfulAccountCreation() {
        ShopPage.findRegisterForm();
        ShopRegistrationPage.register("", "", "", "", "", "");
        CommonVerification.verifyMissingParameters("First Name must be between 1 and 32 characters!", "Something went wrong");
        ShopRegistrationPage.subscribeAndAccept();
    }

    /**
     * Close browser
     */
    @AfterMethod
    public void tearDown() {
        Browser.quit();
    }
}
