package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class Browser {
    public static WebDriver driver;

    public static void open(String typeOfBrowser) {
        switch (typeOfBrowser) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", "src\\driver\\chromedriver\\chromedriver_v78.0.3904.70.exe");
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
                break;
            case "firefox":
                System.setProperty("webdriver.chrome.driver", "src\\driver\\geckodriver\\geckodriver_v0.26.0.exe");
                driver = new FirefoxDriver();
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
                break;
            default:
                throw new RuntimeException("The chosen browser not exist as part of the project content");
        }
    }

    public static void quit() {
        driver.quit();
    }

    public static void goTo() {
        Browser.driver.get("http://shop.pragmatic.bg");
    }
}
