package utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.testng.Assert.assertEquals;

public class CommonVerification {

    public static void verifyCreatedAccount(String expectedMessage, String messageInCaseOfFail) {
        String actualMessage = Browser.driver.getTitle();
        assertEquals(actualMessage, expectedMessage, messageInCaseOfFail);
    }

    public static void verifyMissingParameters(String expectedCondition, String messageInCaseOfFail) {
        String actualMessage = Browser.driver.findElement(By.xpath("//div[@class='text-danger']")).getText();
        assertEquals(actualMessage, expectedCondition, messageInCaseOfFail);
    }
}
