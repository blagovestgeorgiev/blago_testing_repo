package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import utilities.Browser;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ShopPage {
    private static final By LOC_FIND_MYACCOUNT = By.xpath("//i[@class='fa fa-user']");
    private static final By LOC_FIND_REGISTER = By.linkText("Register");


    public static void findRegisterForm() {
        Browser.driver.findElement(LOC_FIND_MYACCOUNT).click();
        Browser.driver.findElement(LOC_FIND_REGISTER).click();
    }


}
