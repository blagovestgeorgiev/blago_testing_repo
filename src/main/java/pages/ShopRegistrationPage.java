package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import utilities.Browser;

public class ShopRegistrationPage {

    private static final By LOC_FIRSTNAME = By.id("input-firstname");
    private static final By LOC_LASTNAME = By.id("input-lastname");
    private static final By LOC_EMAIL = By.id("input-email");
    private static final By LOC_PHONE = By.id("input-telephone");
    private static final By LOC_PASSWORD = By.id("input-password");
    private static final By LOC_PASSWORD_CONFIRM = By.id("input-confirm");
    private static final By LOC_SUBSCRIBE = By.xpath("//input[@type='radio']");
    private static final By LOC_ACCEPT_TERMS = By.xpath("//input[@type='checkbox']");
    private static final By LOC_CONTINUE = By.xpath("//input[@value='Continue']");

    public static void register(String firstname, String lastname, String email, String phone, String password, String passwordConfirm) {
        Browser.driver.findElement(LOC_FIRSTNAME).sendKeys(firstname);
        Browser.driver.findElement(LOC_LASTNAME).sendKeys(lastname);
        Browser.driver.findElement(LOC_EMAIL).sendKeys(email);
        Browser.driver.findElement(LOC_PHONE).sendKeys(phone);
        Browser.driver.findElement(LOC_PASSWORD).sendKeys(password);
        Browser.driver.findElement(LOC_PASSWORD_CONFIRM).sendKeys(passwordConfirm);
    }

    public static void subscribeAndAccept() {
        WebElement newsLetterOn = Browser.driver.findElement(LOC_SUBSCRIBE);
        if(!newsLetterOn.isSelected()){
            newsLetterOn.click();
        }
        Assert.assertTrue(newsLetterOn.isSelected());


        Browser.driver.findElement(LOC_ACCEPT_TERMS).click();
        WebElement termsAndCondOn = Browser.driver.findElement(LOC_ACCEPT_TERMS);
        if (!termsAndCondOn.isSelected()){
            termsAndCondOn.click();
        }
        Assert.assertTrue(termsAndCondOn.isSelected());

        Browser.driver.findElement(LOC_CONTINUE).click();
    }
}
